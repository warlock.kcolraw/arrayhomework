#include <iostream>
#include <ctime>

int main()
{
	const int N = 5;
	int IntArray[N][N];

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			IntArray[i][j] = i + j;
			std::cout << IntArray[i][j] << " ";
		}
		std::cout << "\n";
	}

	int Sum = 0;
	time_t RawTime = time(&RawTime);
	struct tm TimeInfo;

	localtime_s(&TimeInfo, &RawTime);
	for (int j = 0; j < N; j++)
	{
		Sum += IntArray[TimeInfo.tm_mday % N][j];
	}
	std::cout << Sum << "\n";

	return 0;
}